package com.androidformify.forms;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * This abstract class is used to map any value to be modified in a form.
 * Basically, when you prepare your form, for each line in the form, you have :
 *  A text to display
 *  An existing value (can be null)
 *  A type of form to be used by android
 *  Possible values to be submitted to the user (optional)
 *
 *  For each type of form (text, number, spinner...) there is a subclass of FormEntry that MUST be
 *  used.
 *
 * Created by Jeremy on 9/14/2015.
 */
public abstract class FormEntry <V>{
    protected String entryName;
    protected V entryValue;
    protected FormTypeEnum entryType;
    protected ArrayList<String> optionnalValues;
    protected V returnEntry;
    protected MyFormEntryViewHolder holder;

    public FormEntry (){}

    /**
     * Contructor for cases other than spinner. If called and passing a spinner in the Enum argument,
     * will return an Exception;
     * @param pName
     * @param pValue
     */
    public FormEntry (String pName, V pValue) throws IllegalArgumentException{
        entryName = pName;
        entryValue = pValue;
    }

    /**
     * Constructor to be used for spinner types.
     * @param pName
     * @param pValue
     * @param pOptionnalValues
     */
    public FormEntry (String pName, V pValue, ArrayList<String> pOptionnalValues){
        entryName = pName;
        entryValue = pValue;
        optionnalValues = pOptionnalValues;
    }

    public String getEntryName() {
        return entryName;
    }

    public void setEntryName(String entryName) {
        this.entryName = entryName;
    }

    public enum FormTypeEnum {
        StringEditText,
        Spinner,
        NumEditText

    }

    public FormTypeEnum getEntryType() {
        return entryType;
    }

    public ArrayList<String> getOptionnalValues() {
        return optionnalValues;
    }

    public V getEntryValue() {
        return entryValue;
    }

    public void setEntryValue(V entryValue) {
        this.entryValue = entryValue;
    }

    public void setOptionnalValues(ArrayList<String> optionnalValues) {
        this.optionnalValues = optionnalValues;
    }

    /**
     * To be called by the Adapter on each FormEntry. When subclassed, this is supposed to returned
     * the appropriate built view after it was inflated using the given inflater.
     * @param pView
     * @param pInflater
     * @param parent
     * @param pContext
     * @return
     */
    public abstract View getView (View pView, LayoutInflater pInflater, ViewGroup parent, Context pContext);

    public V getReturnEntry(){
        return returnEntry;
    }

    /**
     * Called by the Adapter when the form is saved. since each form entry keeps its own instance
     * of MyFormViewHolder, each form entry is capable of getting the updated value on its own.
     * @return
     */
    public abstract void setReturnEntry ();
}
