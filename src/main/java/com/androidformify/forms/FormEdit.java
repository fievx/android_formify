package com.androidformify.forms;

/**
 * Created by Jeremy on 9/14/2015.
 * @deprecated  should no be used.
 */
@Deprecated
public interface FormEdit {
    /**
     * @deprecated
     */
    @Deprecated
    public void toForm();

}
