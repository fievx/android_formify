package com.androidformify.forms;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Jeremy on 9/27/2015.
 */
public interface Formifyable extends Parcelable {
    /**
     * Used to map from the Object to the the Form.
     * @param pContext is given in case any element needs to be retrived to help map.
     *                 Usually will not be used;
     * @return
     */
    public FormEntry [] fromObjectToForm (Context pContext);

    /**
     * Used to map back from the form to the Object.
     * @param pContext is given in case any element needs to be retrived to help remap.
     *                 Usually will not be used;
     * @param entries All the FormEntries to be remaped to the Object. The entries are in the
     *                same order as given in fromObjectToForm. Be carefull with that.
     */
    void fromFormToObject (Context pContext, FormEntry [] entries);
}
