package com.androidformify.forms;

import android.widget.TextView;

/**
 * Created by Jeremy on 10/8/2015.
 */
public class MyFormEntryViewHolder <V>{
    public TextView text;
    public V view;
}
