package com.androidformify.forms;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.androidformify.R;


/**
 * At minimum the child fragmet must override onSave and onCancel
 *
 * Created by Jeremy on 9/14/2015.
 */
public class BaseFormFragment extends Fragment {
    protected RelativeLayout layout;
    protected Formifyable formObject;
    protected LinearLayout formLayout;
    protected FormEntry [] entriesArray;
    protected MyFormAdapter mAdapter;
    private Button saveButton, cancelButton;

    public static BaseFormFragment getInstance (Formifyable pObject){
        Bundle bundle = new Bundle();
        bundle.putParcelable("parcel", pObject);

        BaseFormFragment frag = new BaseFormFragment();
        frag.setArguments(bundle);
        return frag;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState!=null){
            formObject = savedInstanceState.getParcelable("parcel");
        }
        else {
            formObject = getArguments().getParcelable("parcel");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        layout = (RelativeLayout) inflater.inflate(R.layout.form_base_fragment, container, false);
        formLayout = (LinearLayout) layout.findViewById(R.id.form_layout);
        saveButton = (Button) layout.findViewById(R.id.button_save);
        cancelButton = (Button) layout.findViewById(R.id.button_cancel);

        //We build the formObject and the adapter.
        entriesArray = formObject.fromObjectToForm(getActivity());
        mAdapter = new MyFormAdapter(getActivity(), 0, entriesArray);

        for (int i = 0; i < mAdapter.getCount(); i++) {
            formLayout.addView(mAdapter.getView(i, null, formLayout));
            Log.d("LOG", "View" + i + " added");
        }
        return layout;
    }

    /**
     * Called when the fragment is visible to the user and actively running.
     */
    @Override
    public void onResume() {
        super.onResume();

        //set the listeners on the Buttons
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSave();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancel();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //We save the formObject which is the Fomifyable object
        outState.putParcelable("parcel", formObject);

        //save
//        for (int i =0; i<mAdapter.getCount(); i++){
//            View v = formLayout.getChildAt(i);
//            entriesArray [i] = mAdapter.getFormEntryAtViewPosition(i, v);
//        }
    }

    /**
     * Implements the logic when the users clicks save. We remap the fields back to the FormEntry
     * array and save it in the Formifyable Object using its callback method
     *
     * should be overridden if anything else than saving the object is needed. if overridden, super
     * must be called first. The modified Formifyable object can then be obtained by calling
     * super.getFormObject()
     */
    public void onSave (){
        for (int i =0; i<mAdapter.getCount(); i++){
            View v = formLayout.getChildAt(i);
            entriesArray [i] = mAdapter.getFormEntryAtViewPosition(i, v);
        }

        //We save the new values to the Formifyable Object using its fromFromToObject method
        formObject.fromFormToObject(getActivity(), entriesArray);
    }

    /**
     * Method to be overriden to implement the logic when the user click cancel.
     * Default implementation does not do anything.
     */
    public void onCancel (){

    }

    /**
     * Returns the Object that was passed to create the Fragment. If called after super.OnSave(),
     * the returned object will be the modified version of the object.
     * In the overriden onSave() method, it should be called like this
     * ExpectedObject mObject = (ExpectedObject) super.getFormObject();
     * @return
     */
    public Formifyable getFormObject() {
        return formObject;
    }
}
