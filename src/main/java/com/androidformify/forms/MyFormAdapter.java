package com.androidformify.forms;

import android.app.Activity;
import android.content.Context;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidformify.R;
import com.formEntrySubclasses.NumericalEntry;
import com.formEntrySubclasses.SpinnerEntry;
import com.formEntrySubclasses.TextEntry;

/**
 * Created by Jeremy on 9/15/2015.
 */
public class MyFormAdapter extends ArrayAdapter{

        FormEntry[] formArray;
        Context myContext;
        int layoutResourceId;
        String spinnerSelection;

        public MyFormAdapter(Context context, int resource, Object[] objects) {
            super(context, resource, objects);
            myContext = context;
            layoutResourceId = resource;
            formArray = (FormEntry[])objects;
        }

        @Override
        public int getCount() {
            return formArray.length;
        }


        /**
         * Returns the view at the given position. This is the method where we determine the type of
         * form to be used at each line and we pre-populate it with the current value in the Object.
         * @param position
         * @param convertView can be null
         * @param parent
         * @return
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            MyFormEntryViewHolder holder = null;
            FormEntry entry = formArray[position];
            LayoutInflater inflater = ((Activity)myContext).getLayoutInflater();

            if (row !=null)
                holder = (MyFormEntryViewHolder)row.getTag();
            else {
                row = entry.getView(convertView,inflater, parent, myContext);
                row.setTag(holder);
            }
            return row;
            }

    /**
     * Return the updated FormEntry based on user changes at the given position.
     * @param position
     * @return
     */
    public FormEntry getFormEntryAtViewPosition (int position, View row) throws IndexOutOfBoundsException {
        if (position < 0 || position >formArray.length)
            throw new IndexOutOfBoundsException("Given Adapter Position is out of bounds");

        FormEntry entry = formArray[position];
        entry.setReturnEntry(); //Tells the FormEntry that the time has come to look at the
        //updated values in the View Holder and save them.
        return entry;
    }

}
