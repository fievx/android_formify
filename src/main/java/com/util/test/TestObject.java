package com.util.test;

import android.content.Context;
import android.os.Parcel;

import com.androidformify.forms.FormEntry;
import com.androidformify.forms.Formifyable;
import com.formEntrySubclasses.TextEntry;

import java.util.ArrayList;

/**
 * Created by Jeremy on 10/9/2015.
 */
public class TestObject implements Formifyable {
    String name = "Test";
    int size = 10, speed = 20;
    boolean bool = false;
    ArrayList <String> stringList = new ArrayList<String>();

    public TestObject (Parcel in){
        name = in.readString();
        size = in.readInt();
        speed = in.readInt();
        stringList = in.readArrayList(null);
    }

    public TestObject (){
        for (int i = 0 ;i < 5; i++){
            stringList.add("test"+i);
        }
    }


    @Override
    public FormEntry[] fromObjectToForm(Context pContext) {
        FormEntry[] entriesArray = new FormEntry[3];
        entriesArray [0] = new TextEntry(name, name);

        return entriesArray;
    }

    @Override
    public void fromFormToObject(Context pContext, FormEntry[] entries) {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(size);
        dest.writeInt(speed);
        String [] array = stringList.toArray(new String[stringList.size()]);
        dest.writeStringArray(array);
    }
}
