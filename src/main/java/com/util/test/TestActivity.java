package com.util.test;

import android.app.Activity;
import android.os.Bundle;

import com.androidformify.R;

/**
 * Created by Jeremy on 10/9/2015.
 */
public class TestActivity extends Activity{
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_fortests);
    }
}
