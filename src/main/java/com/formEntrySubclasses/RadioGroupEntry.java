package com.formEntrySubclasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.androidformify.R;
import com.androidformify.forms.FormEntry;
import com.androidformify.forms.MyFormEntryViewHolder;

import java.util.ArrayList;

/**
 * Created by Jeremy on 10/8/2015.
 */
public class RadioGroupEntry extends FormEntry <Integer>{

    public RadioGroupEntry (String pName, Integer pValue, ArrayList<String> pOptionnalValues){
        super (pName, pValue, pOptionnalValues);
    }

    /**
     * To be called by the Adapter on each FormEntry. When subclassed, this is supposed to returned
     * the appropriate built view after it was inflated using the given inflater.
     *
     * @param pView
     * @param pInflater
     * @param parent
     * @param pContext
     * @return
     */
    @Override
    public View getView(View pView, LayoutInflater pInflater, ViewGroup parent, Context pContext) {
        View row = pView;
        holder = new MyFormEntryViewHolder<RadioGroup>();
        row = pInflater.inflate(R.layout.form_element_editor_radio, parent, false);
        holder.view = (RadioGroup) row.findViewById(R.id.entry_value_radio);
        holder.text = (TextView) row.findViewById(R.id.entry_name_radio);
        holder.text.setText(this.getEntryName());
        try {
            //we add the radio buttons to the group
            for (int i =0; i<optionnalValues.size(); i++){
                RadioButton radioButton = new RadioButton(pContext);
                radioButton.setText(optionnalValues.get(i));
                radioButton.setId(i); //we apply an Id to each button. the id is simply its position in the group starting at 0
                ((RadioGroup) holder.view).addView(radioButton, i);
            }
            ((RadioGroup) holder.view).check(entryValue);
        } catch (Exception e) {
            e.printStackTrace();
            ((CheckBox) holder.view).setChecked(false);
        }

        return row;
    }

    /**
     * Called by the Adapter when the form is saved. since each form entry keeps its own instance
     * of MyFormViewHolder, each form entry is capable of getting the updated value on its own.
     *
     * @return
     */
    @Override
    public void setReturnEntry() {
        //We can't get the Id of the checked button because we don't know what ids are assigned to
        //each radio button. Instead we can browse the radio group and stop once we hit the
        //checked one.
        RadioGroup group = (RadioGroup) holder.view;
        //we get the id of the checked button and return it. We can do this because we assigned the ids
        //ourselves and the ids correspond to the position of the button in the list of optional values.
        returnEntry = group.getCheckedRadioButtonId();
    }
}
