package com.formEntrySubclasses;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.androidformify.R;
import com.androidformify.forms.FormEntry;
import com.androidformify.forms.MyFormAdapter;
import com.androidformify.forms.MyFormEntryViewHolder;

import java.util.zip.Inflater;

/**
 * Created by Jeremy on 10/7/2015.
 */
public class TextEntry extends FormEntry <String>{


    public TextEntry(String pName, String pValue){
        super(pName, pValue);
        entryType = FormTypeEnum.StringEditText;
    }

    @Override
    public String getEntryName() {
        return entryName;
    }

    @Override
    public void setEntryName(String entryName) {
        this.entryName = entryName;
    }

    @Override
    public String getEntryValue() {
        return entryValue;
    }

    @Override
    public void setEntryValue(String entryValue) {
        this.entryValue = entryValue;
    }

    @Override
    public View getView(View pView, LayoutInflater pInflater, ViewGroup parent, Context pContext) {
        View row = pView;
        holder = new MyFormEntryViewHolder <EditText>();
        row = pInflater.inflate(R.layout.form_element_editor_text, parent, false);
        holder.view = (EditText) row.findViewById(R.id.entry_value_view);
        holder.text = (TextView) row.findViewById(R.id.entry_name_view);
        holder.text.setText(this.getEntryName());
        try {
            ((EditText) holder.view).setText((String) this.getEntryValue());
        } catch (Exception e) {
            e.printStackTrace();
            ((EditText) holder.view).setText("");
        }

        return row;
    }

    @Override
    public void setReturnEntry() {
        returnEntry = ((EditText) holder.view).getText().toString();
        Log.d("LOG", "return entry has been modified");
    }
}
