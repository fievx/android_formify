package com.formEntrySubclasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.androidformify.R;
import com.androidformify.forms.FormEntry;
import com.androidformify.forms.MyFormEntryViewHolder;

/**
 * Created by Jeremy on 10/8/2015.
 */
public class CheckBoxEntry extends FormEntry <Boolean>{

    /**
     * To be called by the Adapter on each FormEntry. When subclassed, this is supposed to returned
     * the appropriate built view after it was inflated using the given inflater.
     *
     * @param pView
     * @param pInflater
     * @param parent
     * @param pContext
     * @return
     */
    @Override
    public View getView(View pView, LayoutInflater pInflater, ViewGroup parent, Context pContext) {
        View row = pView;
        holder = new MyFormEntryViewHolder<CheckBox>();
        row = pInflater.inflate(R.layout.form_element_editor_check_box, parent, false);
        holder.view = (CheckBox) row.findViewById(R.id.entry_value_check_box);
        holder.text = (TextView) row.findViewById(R.id.entry_name_check_box);
        holder.text.setText(this.getEntryName());
        try {
            ((CheckBox) holder.view).setChecked((Boolean) this.getEntryValue());
        } catch (Exception e) {
            e.printStackTrace();
            ((CheckBox) holder.view).setChecked(false);
        }

        return row;
    }

    /**
     * Called by the Adapter when the form is saved. since each form entry keeps its own instance
     * of MyFormViewHolder, each form entry is capable of getting the updated value on its own.
     *
     * @return
     */
    @Override
    public void setReturnEntry() {
        returnEntry = ((CheckBox) holder.view).isChecked();
    }
}
