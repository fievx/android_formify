package com.formEntrySubclasses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidformify.R;
import com.androidformify.forms.FormEntry;
import com.androidformify.forms.MyFormAdapter;
import com.androidformify.forms.MyFormEntryViewHolder;

import java.util.ArrayList;

/**
 * Created by Jeremy on 10/7/2015.
 */
public class SpinnerEntry extends FormEntry <String>{

    public SpinnerEntry (String pName, String pValue, ArrayList <String> pOptionList){
        super(pName, pValue, pOptionList);
        entryType = FormTypeEnum.Spinner;
    }

    @Override
    public View getView(View pView, LayoutInflater pInflater, ViewGroup parent, Context pContext) {
        Context myContext = pContext;

        View row = pView;
        row = pInflater.inflate(R.layout.form_element_editor_spinner, parent, false);

        holder = new MyFormEntryViewHolder<Spinner>();
        holder.view = (Spinner) row.findViewById(R.id.entry_value_spinner);
        holder.text = (TextView) row.findViewById(R.id.entry_name_view);
        holder.text.setText(this.getEntryName());

        ArrayAdapter<String> spinAdapter = new ArrayAdapter<String>(myContext, R.layout.simple_spinner_item,
                this.getOptionnalValues());
        spinAdapter.setDropDownViewResource(R.layout.simple_spinner_item);
        ((Spinner) holder.view).setAdapter(spinAdapter);

        //We determine the previous value of the Object and set it in the Spinner
        int spinnerPosition = this.getOptionnalValues().indexOf(this.getEntryValue());
        ((Spinner) holder.view).setSelection(spinnerPosition);

        //we set the listener
        ((Spinner) holder.view).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //record the selected value in a String
                returnEntry = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return row;
    }

    /**
     * Does not do anything because the return entry is already saved when the user changes the
     * value of the spinner. (There is an OnItemSeletectedListener)
     * @param row
     * @return
     */
    @Override
    public void setReturnEntry() {
    }


}
