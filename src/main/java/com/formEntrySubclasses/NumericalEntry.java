package com.formEntrySubclasses;

import android.content.Context;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.androidformify.R;
import com.androidformify.forms.FormEntry;
import com.androidformify.forms.MyFormAdapter;
import com.androidformify.forms.MyFormEntryViewHolder;

import java.util.ArrayList;

/**
 * Created by Jeremy on 10/7/2015.
 */
public class NumericalEntry extends FormEntry <Integer> {


    public NumericalEntry (String pName, int pValue){
        super(pName, pValue);
        entryType = FormTypeEnum.NumEditText;
    }

    @Override
    public Integer getEntryValue() {
        return super.getEntryValue();
    }

    @Override
    public View getView(View pView, LayoutInflater pInflater, ViewGroup parent, Context pContext) {
        View row = pView;
        row = pInflater.inflate(R.layout.form_element_editor_numeric, parent, false);

        holder = new MyFormEntryViewHolder<EditText>();
        holder.view = (EditText) row.findViewById(R.id.entry_value_numeric);
        holder.text = (TextView) row.findViewById(R.id.entry_name_numeric);
        holder.text.setText(this.getEntryName());
        ((EditText) holder.view).setInputType(InputType.TYPE_CLASS_NUMBER);

        try {
            ((EditText) holder.view).setText(String.valueOf(this.getEntryValue()));
        } catch (Exception e) {
            e.printStackTrace();
            ((EditText) holder.view).setText("");
        }
        return row;
    }

    /**
     * @param row
     * @return
     */
    @Override
    public void setReturnEntry() {
        returnEntry = Integer.valueOf(((EditText) holder.view).getText().toString());
    }
}
