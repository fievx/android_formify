package EntrySubclasses;

import android.app.FragmentTransaction;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;

import com.androidformify.R;
import com.androidformify.forms.BaseFormFragment;
import com.formEntrySubclasses.TextEntry;
import com.util.test.TestActivity;
import com.util.test.TestObject;

/**
 * Created by Jeremy on 10/9/2015.
 */
public class TextEntryTest extends ActivityInstrumentationTestCase2 <TestActivity>{
    TextEntry entry;
    String name = "Test Name", value = "test value";
    View mockView;
    TestActivity activity;
    TestObject testObject  = new TestObject();
    BaseFormFragment frag;

    public TextEntryTest (){
        super(TestActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        activity = getActivity();

        //we add the BaseFormFragment to the Activity
        frag = BaseFormFragment.getInstance(testObject);
        FragmentTransaction transaction = activity.getFragmentManager().beginTransaction();
        transaction.add(R.id.activity_test_fragment_linearlayout, frag, "tag").commit();

        frag = (BaseFormFragment) activity.getFragmentManager().findFragmentByTag("tag");
    }

    public void testBaseFormFragmentIsDisplayed() throws Exception {
        assertTrue(!frag.isHidden());
    }
}
